//
//  TimerViewController.swift
//  Boogie.FM
//
//  Created by Tino Krželj on 22/09/2017.
//  Copyright © 2017 Timur Besirovic. All rights reserved.
//

import UIKit

class TimerViewController: UIViewController {
    
    // MARK: - PROPERTIES
    
    enum CountdownTimerButtonType: Int {
        case start = 0
        case stop = 1
    }
    
    lazy var countdownTimer = Timer()
    lazy var selectedTime = Double()
    var defaultUpperConstraintForCountdownTimerContainerView = 40
    
    // MARK: - IBOUTLETS
    
    @IBOutlet private weak var countdownTimerContainerView: UIView!
    @IBOutlet private weak var countdownTimePickerView: UIDatePicker!
    @IBOutlet private weak var startCountdownTimeButton: UIButton!
    @IBOutlet private weak var timerPresenterContainerView: UIView!
    @IBOutlet private weak var timerPresenterLabel: UILabel!
    
    @IBOutlet weak var countdownTimerContainerViewUpperConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var timePresenterContainerUpperConstraint: NSLayoutConstraint!
    // MARK: - IBACTIONS
    
    @IBAction private func startCountdownTimerButtonPressed() {
        if startCountdownTimeButton.tag == CountdownTimerButtonType.start.rawValue {
            setStartCountdownButtonState(tag: CountdownTimerButtonType.stop.rawValue)
            
            let countdownTimeInterval = countdownTimePickerView.countDownDuration
            selectedTime = Double(countdownTimeInterval)

            countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(TimerViewController.presentCurrentTime), userInfo: nil, repeats: true)
            
            animateTimerContainerView(withUpperConstraintValue: -1000, withAnimation: true)
            animateTimePresenterContainerView(withUpperConstraintValue: CGFloat(defaultUpperConstraintForCountdownTimerContainerView), withAnimation: true)
            animateTimePresenterContainerViewShowing(wantToShow: true, withAnimation: true)
        } else {
            setStartCountdownButtonState(tag: CountdownTimerButtonType.start.rawValue)
            invalidateTimerWithDataUpdateWithAnimation()
        }
    }
    
    // MARK: - CUSTOM METHODS
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(TimerViewController.isStreamPlayingStatusChanged), name: ApplicationNotificationIdentifiers.isStreamPlayingStatusChanged, object: nil)
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, queue: nil) { notification in
            self.applicationDidBecomeActive()
        }
    }
    
    private func setDefaultValues() {
        view.backgroundColor = ColorScheme.defaultBackground
        countdownTimePickerView.countDownDuration = 1.0
        countdownTimerContainerView.backgroundColor = ColorScheme.whiteTransparent
        countdownTimerContainerView.setCornerRadius(valueToSet: 8.0)
        countdownTimePickerView.setValue(UIColor.black, forKey: "textColor")
        startCountdownTimeButton.setDefaultBackground()
        startCountdownTimeButton.setCornerRadius(valueToSet: 8.0)
        startCountdownTimeButton.setTitle(ApplicationStrings.startCountdownTimerButtonTitle, for: .normal)
        startCountdownTimeButton.setTitleColor(UIColor.black, for: .normal)
        timerPresenterContainerView.backgroundColor = ColorScheme.whiteTransparent
        timerPresenterContainerView.setCornerRadius(valueToSet: 8.0)
        timerPresenterLabel.textColor = UIColor.black
        animateTimePresenterContainerViewShowing(wantToShow: false, withAnimation: false)
    }
    
    private func checkTimerButtonState() {
        if StreamService.shared.isStreamPlaying {
            startCountdownTimeButton.buttonState(isButtonEnabled: true)
        } else {
            startCountdownTimeButton.buttonState(isButtonEnabled: false)
        }
    }
    
    private func secondsToHoursMinutesSeconds (seconds : Int) -> (hours: Int, minutes: Int, seconds: Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    private func getCorrectCountdownTime(forSeconds currentSeconds: Double) -> String {
        var hours = ""
        var minutes = ""
        var seconds = ""
        var stringTime = String()
        let result = secondsToHoursMinutesSeconds(seconds: Int(currentSeconds))
        
        if result.hours > 0 {
            if result.hours < 10 { hours = "0\(result.hours):" } else { hours = "\(result.hours):" }
            if result.minutes < 10 { minutes = "0\(result.minutes):" } else { minutes = "\(result.minutes):" }
            if result.seconds < 10 { seconds = "0\(result.seconds)" } else { seconds = "\(result.seconds)" }
            
            stringTime = "\(hours)\(minutes)\(seconds)"
        } else {
            
            if result.minutes < 10 { minutes = "0\(result.minutes):" } else { minutes = "\(result.minutes):" }
            if result.seconds < 10 { seconds = "0\(result.seconds)" } else { seconds = "\(result.seconds)" }
            
            stringTime = "\(minutes)\(seconds)"
        }
        
        return stringTime
    }
    
    func setStartCountdownButtonState(tag: Int) {
        if tag == 0 {
            startCountdownTimeButton.tag = 0
            startCountdownTimeButton.setTitle(ApplicationStrings.startCountdownTimerButtonTitle, for: .normal)
        } else {
            startCountdownTimeButton.tag = 1
            startCountdownTimeButton.setTitle(ApplicationStrings.stopCountdownTimerButtonTitle, for: .normal)
        }
    }
    
    // MARK: - SELECTOR METHODS
    
    @objc func isStreamPlayingStatusChanged() {
        checkTimerButtonState()
    }
    
    @objc func applicationDidBecomeActive() {
        checkTimerButtonState()
        if !StreamService.shared.isStreamPlaying {
            setStartCountdownButtonState(tag: CountdownTimerButtonType.start.rawValue)
        }
    }
    
    func invalidateTimerWithDataUpdateWithAnimation() {
        countdownTimer.invalidate()
        animateTimerContainerView(withUpperConstraintValue: CGFloat(defaultUpperConstraintForCountdownTimerContainerView), withAnimation: true)
        animateTimePresenterContainerView(withUpperConstraintValue: 400.0, withAnimation: true)
        animateTimePresenterContainerViewShowing(wantToShow: false, withAnimation: true)
        timerPresenterLabel.text = ""
    }
    
    func invalidateTimerWithDataUpdateWithoutAnimation() {
        countdownTimer.invalidate()
        animateTimerContainerView(withUpperConstraintValue: CGFloat(defaultUpperConstraintForCountdownTimerContainerView), withAnimation: false)
        animateTimePresenterContainerView(withUpperConstraintValue: 400.0, withAnimation: false)
        animateTimePresenterContainerViewShowing(wantToShow: false, withAnimation: false)
        timerPresenterLabel.text = ""
    }
    
    @objc func presentCurrentTime() {
        selectedTime -= 1
        
        #if DEBUG
            print("TIMER_VC_COUNTDOWN = ", getCorrectCountdownTime(forSeconds: selectedTime))
        #endif
        
        timerPresenterLabel.text = "\(getCorrectCountdownTime(forSeconds: selectedTime))"
        
        if selectedTime == 0 {
            StreamService.shared.stopStream()
            invalidateTimerWithDataUpdateWithAnimation()
        }
        
    }
    
    // MARK: - VIEW METHODS
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkTimerButtonState()
        if !StreamService.shared.isStreamPlaying {
            invalidateTimerWithDataUpdateWithoutAnimation()
            setStartCountdownButtonState(tag: CountdownTimerButtonType.start.rawValue)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        registerForNotifications()
        setDefaultValues()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}

// MARK: - EXTENSION - ANIMATIONS

extension TimerViewController {
    private func animateTimerContainerView(withUpperConstraintValue value: CGFloat, withAnimation animate: Bool) {
        if animate {
            UIView.animate(withDuration: 2.0) {
                self.countdownTimerContainerViewUpperConstraint.constant = value
                self.view.layoutIfNeeded()
            }
        } else {
            self.countdownTimerContainerViewUpperConstraint.constant = value
        }
    }
    
    private func animateTimePresenterContainerView(withUpperConstraintValue value: CGFloat, withAnimation animate: Bool) {
        if animate {
            UIView.animate(withDuration: 2.0) {
                self.timePresenterContainerUpperConstraint.constant = value
                self.view.layoutIfNeeded()
            }
        } else {
            self.timePresenterContainerUpperConstraint.constant = value
        }
    }
    
    private func animateTimePresenterContainerViewShowing(wantToShow show: Bool, withAnimation animate: Bool) {
        if animate {
            UIView.animate(withDuration: 2.0) {
                if show {
                    self.timerPresenterContainerView.alpha = 1.0
                    self.view.layoutIfNeeded()
                } else {
                    self.timerPresenterContainerView.alpha = 0.0
                    self.view.layoutIfNeeded()
                }
            }
        } else {
            if show {
                timerPresenterContainerView.alpha = 1.0
            } else {
                timerPresenterContainerView.alpha = 0.0
            }
        }
    }
    
    
}

//
//  ViewController.swift
//  Boogie.FM
//
//  Created by Timur Besirovic on 17/06/2017.
//  Copyright © 2017 Timur Besirovic. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Canvas
import MarqueeLabel
import MediaPlayer

class ViewController: UIViewController {
    
    var isPlaying = false
    var isSetted = false
    
    @IBOutlet var volumeView: CustomMPVolume!
    @IBOutlet weak var wave: WaveView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var playButtonView: CSAnimationView!
    @IBOutlet weak var titleLabel: MarqueeLabel!
    @IBOutlet weak var boogieLogoImageView: UIImageView!
    @IBOutlet weak var boogieCircleOne: UIImageView!
    @IBOutlet weak var boogieCircleTwo: UIImageView!
    @IBOutlet weak var boogieFirstCircleLeadingConstrain: NSLayoutConstraint!
    @IBOutlet weak var shareButton: UIButton!
    
    @IBAction func playButtonPressed(_ sender: Any) {
        isPlaying = !isPlaying
        playButtonView.startCanvasAnimation()
        setStreamVCDefaultState(isStreamPlaying: isPlaying)
        
        if isPlaying {
            StreamService.shared.playStream()
            if titleLabel.text == "" { titleLabel.text = ApplicationStrings.playingStream }
            animateCircles()
            
            UIApplication.shared.beginReceivingRemoteControlEvents()
            self.becomeFirstResponder()
            
            MPNowPlayingInfoCenter.default().playbackState = .playing
            MPNowPlayingInfoCenter.default().nowPlayingInfo = [
                MPMediaItemPropertyArtist: "Live",
                MPMediaItemPropertyTitle: "BoogieON Radio",
                MPMediaItemPropertyPlaybackDuration: "1.0"
            ]
        } else {
            MPNowPlayingInfoCenter.default().playbackState = .paused
            StreamService.shared.stopStream()
            stopCirclesAnimations()
        }
    }
    
    
    @IBAction func shareButtonPressed(_ sender: Any) {
        var trackName: String
        
        if StreamService.shared.playerItem.timedMetadata != nil {
            trackName = (StreamService.shared.playerItem.timedMetadata![0].value as? String)!
        }else {
            trackName = "great music"
        }
    
        let firstActivityItem = "Listening to \(trackName) on #BoogieOnRadio"
        let secondActivityItem : NSURL = NSURL(string: "https://itunes.apple.com/hr/app/boogie-fm/id1214721505?mt=8")!
        // If you want to put an image
        //let image : UIImage = UIImage(named: "boogieOnLogo")!
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem, secondActivityItem], applicationActivities: nil)
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivityType.postToWeibo,
            UIActivityType.print,
            UIActivityType.assignToContact,
            UIActivityType.saveToCameraRoll,
            UIActivityType.addToReadingList,
            UIActivityType.postToFlickr,
            UIActivityType.postToVimeo,
            UIActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
        
        
    }
    
    // MARK: - CUSTOM METHODS
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.isStreamPlayingStatusChanged), name: ApplicationNotificationIdentifiers.isStreamPlayingStatusChanged, object: nil)
    }
    
    func setDefaultValues() {
        view.backgroundColor = ColorScheme.defaultBackground

        let sz = CGSize(width: 10, height: 10)
        UIGraphicsBeginImageContextWithOptions(CGSize(width: sz.height, height: sz.height), false, 0)
        
        UIColor.black.setFill()
        UIBezierPath(ovalIn:CGRect(x: 0, y: 0, width: sz.height, height: sz.height)).fill()
        
        let im1 = UIGraphicsGetImageFromCurrentImageContext()
        UIBezierPath(ovalIn:CGRect(x: 0, y: 0, width: sz.height, height: sz.height)).fill()
        
        let im2 = UIGraphicsGetImageFromCurrentImageContext()
        UIColor.orange.setFill()
        UIBezierPath(ovalIn:CGRect(x: 0, y: 0, width: sz.height, height: sz.height)).fill()
        
        self.volumeView.setMinimumVolumeSliderImage(
            im1!.resizableImage(withCapInsets: UIEdgeInsetsMake(9,9,9,9),resizingMode:.tile),
            for:.normal)
        self.volumeView.setMaximumVolumeSliderImage(im2!.resizableImage(withCapInsets: UIEdgeInsetsMake(11,11,11,11),resizingMode:.tile),for:.normal)
        
        volumeView.showsRouteButton = false
    }
    
    func playTapped() { StreamService.shared.playStream()}
    @objc func stopTapped() { StreamService.shared.stopStream() }
    
    func setupCircles() {
        switch Display.typeIsLike {
        case DisplayType.iphoneSE: addConstraintToFirstCircle(dividedBy: 4.38)
        case DisplayType.iphone8: addConstraintToFirstCircle(dividedBy: 4.32)
        case DisplayType.iphone8plus: addConstraintToFirstCircle(dividedBy: 4.35)
        default: addConstraintToFirstCircle(dividedBy: 4.05)
        }
    }
    
    private func addConstraintToFirstCircle(dividedBy: CGFloat) {
        boogieFirstCircleLeadingConstrain.constant = boogieLogoImageView.bounds.width / dividedBy
    }
    
    func animateCircles() {
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        
        rotationAnimation.fromValue = 2 * Double.pi
        rotationAnimation.toValue = 0.0
        rotationAnimation.duration = 3.0
        rotationAnimation.repeatCount = .infinity
        boogieCircleOne.layer.add(rotationAnimation, forKey: "transform.rotation")
        boogieCircleTwo.layer.add(rotationAnimation, forKey: "transform.rotation")
        boogieCircleOne.startAnimating()
        boogieCircleTwo.startAnimating()
    }
    
    func stopCirclesAnimations() {
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotationAnimation.fromValue = 2 * Double.pi
        rotationAnimation.toValue = rotationAnimation.fromValue
        rotationAnimation.duration = 1.0
        
        let rotationAnimation2 = CABasicAnimation(keyPath: "transform.rotation")
        rotationAnimation2.fromValue = Double.pi / 2
        rotationAnimation2.toValue = 0
        rotationAnimation2.duration = 0.07
        
        boogieCircleOne.layer.add(rotationAnimation, forKey: "transform.rotation")
        boogieCircleTwo.layer.add(rotationAnimation2, forKey: "transform.rotation")
        
        boogieCircleOne.startAnimating()
        boogieCircleTwo.startAnimating()
    }
    
    // MARK: - SELECTOR METHODS
    
    @objc func isStreamPlayingStatusChanged() {
        isPlaying = StreamService.shared.isStreamPlaying
        setStreamVCDefaultState(isStreamPlaying: isPlaying)
    }
    
    // MARK: - VIEW METHODS
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isPlaying = StreamService.shared.isStreamPlaying
        setStreamVCDefaultState(isStreamPlaying: isPlaying)
        
        if !isSetted {
            setupCircles()
            isSetted = true
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        becomeFirstResponder()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        registerForNotifications()
        setPlayButtonAnimation()
        setDefaultValues()
        isWaveHidden(state: true)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
        
    override var canBecomeFirstResponder: Bool { return true}
    func setPlayButtonAnimation(){
        playButtonView.duration = 0.3
        playButtonView.delay = 0
        playButtonView.type = CSAnimationTypeMorph
        self.view.addSubview(playButtonView)
    }
    
    func isWaveHidden(state: Bool) {
        wave.isHidden = state
    }

    func setStreamVCDefaultState(isStreamPlaying playing: Bool) {
        if playing {
            isWaveHidden(state: false)
            playButton.setImage(UIImage(named: "pause-round-button"), for: .normal)
            StreamService.shared.playerItem.addObserver(self, forKeyPath: "timedMetadata", options: .new, context: nil)
            titleLabel.isEnabled = true
            titleLabel.textColor = .black
            animateCircles()
        } else {
            isWaveHidden(state: true)
            playButton.setImage(UIImage(named: "round-play-button"), for: .normal)
            titleLabel.isEnabled = false
            titleLabel.textColor = ColorScheme.currentSongEnabled
            stopCirclesAnimations()
        }
    }

    override func remoteControlReceived(with event: UIEvent?) {
        if event!.type == UIEventType.remoteControl {
            if event!.subtype == UIEventSubtype.remoteControlPlay {
                #if DEBUG
                    print("received remote play")
                #endif
                StreamService.shared.playStream()

            } else if event!.subtype == UIEventSubtype.remoteControlPause {
                #if DEBUG
                    print("received remote pause")
                #endif
                StreamService.shared.stopStream()

            } else if event!.subtype == UIEventSubtype.remoteControlTogglePlayPause {
                #if DEBUG
                    print("received toggle")
                #endif

            }else if event!.subtype == UIEventSubtype.remoteControlNextTrack{
                #if DEBUG
                    print("received next")
                #endif

            }
        }
    }
}


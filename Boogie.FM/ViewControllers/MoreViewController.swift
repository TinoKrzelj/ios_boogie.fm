//
//  MoreViewController.swift
//  Boogie.FM
//
//  Created by Timur Besirovic on 18/06/2017.
//  Copyright © 2017 Timur Besirovic. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet var shareButtons: [UIButton]!
    @IBOutlet private weak var logoImageView: UIImageView!
    @IBOutlet weak var logoButton: UIButton!
    
    // MARK: - IBActions
    
    @IBAction func moreButtonPressed(sender: UIButton) {
        if sender.tag == 0 {
            // Home
        } else {
            openWebPage(urlString: "https://www.facebook.com/BoogieFM/")
        }
    }
    
    func openWebPage(urlString: String) {
        guard let url = URL(string: urlString) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    // MARK: - VIEW METHODS
        
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ColorScheme.defaultBackground
        
        for button in shareButtons {
            button.setDefaultBackground()
            
            if button.tag == 0 {
                button.setAndCenterImage(imageName: "homePageWhite")
            } else {
                button.setAndCenterImage(imageName: "facebookWhite")
            }
        }
        logoButton.setImage(#imageLiteral(resourceName: "white_logo_transparent_background_small-1").alpha(0.6), for: .normal)
        logoButton.setImage(#imageLiteral(resourceName: "white_logo_transparent_background_small-1"), for: .highlighted)
        logoButton.imageView?.contentMode = .scaleAspectFit
    }
    @IBAction func logoButtonPressed(_ sender: Any) {
        openWebPage(urlString: "http://geopbyte.co/")
    }
    
}



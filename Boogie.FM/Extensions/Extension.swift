//
//  Extension.swift
//  Boogie.FM
//
//  Created by Timur Besirovic on 18/06/2017.
//  Copyright © 2017 Timur Besirovic. All rights reserved.
//

import UIKit


// MARK: - UIButton


extension UIButton {
    func setAndCenterImage(imageName: String) {
        var inset: CGFloat = 0.0
        
        if Display.typeIsLike == .iphoneSE {
            inset = 16.0
        } else if Display.typeIsLike == .iphone8 {
            inset = 20.0
        } else if Display.typeIsLike == .iphone4s {
            inset = 14.0
        } else {
            inset = 24.0
        }
    
        self.setImage(UIImage(named: imageName), for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
        self.imageView?.contentMode = .scaleAspectFit
    }
        
    func buttonState(isButtonEnabled enabled: Bool) {
        if enabled {
            self.alpha = 1.0
            self.isEnabled = true
        } else {
            self.alpha = 0.4
            self.isEnabled = false
        }
    }
}

// MARK: - UIVIEW

extension UIView {
    func setDefaultBackground() {
        self.backgroundColor = ColorScheme.whiteTransparent
    }
    
    func setCornerRadius(valueToSet value: CGFloat) {
        guard value > 0.0 else { return }
        layer.cornerRadius = value
    }
}

// MARK: - UIImageView
extension UIImage {
    
    func alpha(_ value:CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: CGPoint.zero, blendMode: .normal, alpha: value)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}

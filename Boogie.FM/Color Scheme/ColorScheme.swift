//
//  ColorScheme.swift
//  Boogie.FM
//
//  Created by Timur Besirovic on 18/06/2017.
//  Copyright © 2017 Timur Besirovic. All rights reserved.
//

import UIKit

struct ColorScheme {
    static var defaultBackground: UIColor { return UIColor.white }
    static var whiteTransparent: UIColor { return UIColor(red: 0, green: 0, blue: 0, alpha: 0.1) }
    static var currentSongEnabled: UIColor { return UIColor(red:0.84, green:0.84, blue:0.84, alpha:1.00) }
}

//
//  StreamService.swift
//  Boogie.FM
//
//  Created by Tino Krželj on 23/09/2017.
//  Copyright © 2017 Timur Besirovic. All rights reserved.
//

import Foundation
import AVFoundation
import AVKit
import MediaPlayer

class StreamService {
    static let shared = StreamService()
    private init() {
        player = AVPlayer(playerItem: playerItem)
        
    }
    
    // MARK: - PROPERTIES
    
    lazy var player = AVPlayer()
    let playerItem = AVPlayerItem(url: Urls.streamUrl)
    
    var isStreamPlaying = false {
        didSet {
            NotificationCenter.default.post(name: ApplicationNotificationIdentifiers.isStreamPlayingStatusChanged, object: nil)
            #if DEBUG
                print("Is stream playing = ", isStreamPlaying)
            #endif
        }
    }
    
    // MARK: - CUSTOM METHODS
    
    func playStream() {
        player.play()
        isStreamPlaying = true
        
    }
    
    func stopStream() {
        player.pause()
        isStreamPlaying = false
    }
    

    
    
}

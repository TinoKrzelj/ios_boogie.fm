//
//  Constants.swift
//  Boogie.FM
//
//  Created by Timur Besirovic on 17/06/2017.
//  Copyright © 2017 Timur Besirovic. All rights reserved.
//

import Foundation

struct Urls {
    
    static var streamUrl = URL(string: "http://o-7955484728550.oblak.bg/stream")!
}

struct ApplicationNotificationIdentifiers {
    static let isStreamPlayingStatusChanged = NSNotification.Name("isStreamPlayingStatusChanged")
}

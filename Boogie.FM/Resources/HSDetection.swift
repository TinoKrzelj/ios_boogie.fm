//
//  HSDetection.swift
//  SportsBook
//
//  Created by Tino Krzelj on 24/01/2017.
//  Copyright © 2017 Tino Krzelj. All rights reserved.
//

import UIKit

public enum DisplayType {
    case unknown
    case iphone4s // also includes 4
    case iphoneSE // also includes 5, 5s, 5c
    case iphone8 // also includes 6, 6s, 7
    case iphone8plus // also includes 6plus, 6s_plus, 7plus
    case iphoneX
}

public final class Display {
    class var width:CGFloat { return UIScreen.main.bounds.size.width }
    class var height:CGFloat { return UIScreen.main.bounds.size.height }
    class var maxLength:CGFloat { return max(width, height) }
    class var minLength:CGFloat { return min(width, height) }
    class var zoomed:Bool { return UIScreen.main.nativeScale >= UIScreen.main.scale }
    class var retina:Bool { return UIScreen.main.scale >= 2.0 }
    class var phone:Bool { return UIDevice.current.userInterfaceIdiom == .phone }
    class var pad:Bool { return UIDevice.current.userInterfaceIdiom == .pad }
    class var carplay:Bool { return UIDevice.current.userInterfaceIdiom == .carPlay }
    class var tv:Bool { return UIDevice.current.userInterfaceIdiom == .tv }
    class var typeIsLike:DisplayType {
        if phone && maxLength < 568 {
            return .iphone4s
        }
        else if phone && maxLength == 568 {
            return .iphoneSE
        }
        else if phone && maxLength == 667 {
            return .iphone8
        }
        else if phone && maxLength == 736 {
            return .iphone8plus
        } else if phone && maxLength == 812 {
            return .iphoneX
        }
        return .unknown
    }
}

//
//  ApplicationStrings.swift
//  Boogie.FM
//
//  Created by Tino Krželj on 22/09/2017.
//  Copyright © 2017 Timur Besirovic. All rights reserved.
//

import Foundation

struct ApplicationStrings {
    static let startCountdownTimerButtonTitle = "START TIMER"
    static let stopCountdownTimerButtonTitle = "CANCEL TIMER"
    static let playingStream = "#BoogieOn Radio Live "
}

//
//  CustomMPVolume.swift
//  Boogie.FM
//
//  Created by iOS NSoft on 24/09/2017.
//  Copyright © 2017 Timur Besirovic. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MediaPlayer

class CustomMPVolume: MPVolumeView {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.removeAnimations(view: self)
    }
    
    func removeAnimations(view: UIView) {
        view.layer.removeAllAnimations()
        for sub in view.subviews {
            self.removeAnimations(view: sub)
        }
    }
}
